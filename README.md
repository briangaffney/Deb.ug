# Deb.ug

Personal website for Brian Gaffney

Deployed to Netlify.

`master` branch auto-deploys to https://deb.ug

`dev` branch auto deploys to https://dev--deb-ug.netlify.app

## Colours

```
Red RYB: #FF1F22
Ultramarine blue: #3358FF
Pressian blue: #003049
Baby powder: #FDFFFC
Safety yellow: #F1D302
```
