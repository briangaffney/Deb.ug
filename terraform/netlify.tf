resource "netlify_site" "deb-ug" {
  name = "deb.ug"

  custom_domain = "deb.ug"

  repo {
    repo_branch = "master"
    command     = "yarn build"
    dir         = "build"
    provider    = "gitlab"
    repo_path   = "briangaffney/Deb.ug"
  }
}
